


      function sumAmount(){
        var sum = 0;
        $("tr").each(function() {

          //console.log($(this).find('.amount').text());
          
          var val = $(this).attr('data-value');
          var transaction = $(this).find('.transaction').text();
          //var multiplier = transaction = 'Saque' ? -1 : 1;

          if ( val ) {
              val = parseFloat( val.replace( /^\$/, "" ) );
          
              sum += !isNaN( val ) ? val : 0;
          }
          
        });
		sum = sum.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('.sum').html(sum)
      }
	  
	function RemoveNode(){
	 $('.action-remove').click(
        function(){
          $(this).parent().parent().remove();
          sumAmount();
        }
      );
	};
	RemoveNode();
	
	

      $('#add').click(
        function(){
          var amount = $('input[name=amount]').val();
          var transaction = $("input[type='radio']:checked").attr("data-value");
		  
		  if (transaction=='Saque')
			  amount = amount * -1;
		  
           $('tbody').append('<tr class="table-row" data-value="'+amount+'"><td class="transaction">'+transaction+'</td><td class="amount">'+Math.abs(amount)+'</td><td class="text-right"><span class="action-remove">×</span></td></tr>');
           sumAmount();
		   RemoveNode();
       });

      

