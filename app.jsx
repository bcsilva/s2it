console.clear();

const Title = () => {
    return (
        <h1 className="text-center">Teste de Front-end</h1>
    );
}

const Result = () => {
    let total;
    return (
        <h2 class="text-center">Total em conta:<span>R$ 0,00</span></h2>
    );
}


const TodoForm = ({
    addTodo
}) => {
    // Input Tracker
    let input, saque, deposito, radio;
    // Return JSX
    return (
        <div className="form-inline row">
			<div className="col">
				<div className="form-check form-check-inline">
				<label className="form-check-label">
					<input type="radio" className="form-check-input" name="optionsRadios" id="deposito" value="deposito" checked={node => {
				deposito = node;
			  }}/>
					Depósito
				  </label>
				</div>			
			</div>
			<div className="col">
				<div className="form-check form-check-inline">
				  <label className="form-check-label">
					<input type="radio" className="form-check-input" name="optionsRadios" id="saque" value="saque" checked={node => {
				saque = node;
			  }}/>
					Saque
				  </label>
				</div>
			</div>
			<div className="col">
			<div className="form-group">
			<input type="text" className="form-control" id="value" id="value" placeholder="Valor" ref={node => {
				input = node;
			  }} />
			</div>
			</div>
			<div className="col">
			<div className="form-group">			  
			<button className="btn btn-primary" onClick={() => {
				if (input.value!=''){
					radio = deposito.hasAttribute("checked") ? deposito.value : saque.value;
					console.log(radio);
					addTodo(radio,input.value);
					input.value = '';				
				}

			  }}>
				Adicionar
			  </button>
				</div>
			</div>
		</div>
    );
};

const Todo = ({
    todo,
    remove
}) => {
    // Each Todo
    return (<tr className="table-row"><td>{todo.transaction}</td><td>{todo.text}</td><td><span className="action-remove" onClick={() => {remove(todo.id)}}>Remover</span></td></tr>);
}

const TodoList = ({
    todos,
    remove
}) => {
    // Map through the todos
    const todoNode = todos.map((todo) => {
        return (<Todo todo={todo} key={todo.id} remove={remove}/>)
    });
    return (<table className="table table-striped"><thead><tr><th>Tipo</th><th>Valor</th><th></th></tr></thead><tbody>{todoNode}</tbody></table>);
}

// Contaner Component
// Todo Id
window.id = 0;
class TodoApp extends React.Component {
    constructor(props) {
        // Pass props to parent class
        super(props);
        // Set initial state
        this.state = {
            data: [],
        };

    }
    // Add todo handler
    addTodo(transaction,val) {
        // Assemble data
        const todo = {
            transaction: transaction,
            text: val,
            id: window.id++
        }
        // Update data
        this.state.data.push(todo);
        // Update state
        this.setState({
            data: this.state.data
        });
    }
    // Handle remove
    handleRemove(id) {
        // Filter all todos except the one to be removed
        const remainder = this.state.data.filter((todo) => {
            if (todo.id !== id) return todo;
        });
        // Update state with filter
        this.setState({
            data: remainder
        });
    }



    render() {
        // Render JSX
        return (
            <div>
				<Title />
				<TodoForm addTodo={this.addTodo.bind(this)}/>
				<TodoList 
				  todos={this.state.data} 
				  remove={this.handleRemove.bind(this)}
				/>
			  </div>
        );
    }
}
ReactDOM.render(<TodoApp />, document.getElementById('container'));